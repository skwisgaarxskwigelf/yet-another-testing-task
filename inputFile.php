<?php

require_once __DIR__ . "/conn_db.php";

class inputFile
{
    protected $conn;

    /**
     * inputFile constructor.
     */
    public function __construct()
    {
        $this->conn = new PDO('mysql:host=localhost;dbname=test;charset=utf8', DB_USER, DB_PASSWORD);
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * @return array
     */
    private function getArticul(): array
    {
        try {
            $getItems = $this->conn->prepare("SELECT * FROM test ORDER BY id");
            $getItems->execute();
            return $getItems->fetchAll(\PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    /**
     * @return array
     */
    public function prepareCsv(): array
    {
        $res = [];
        $articuls = $this->getArticul();
        $csvContent = array_map('str_getcsv', file('file.csv'));
        foreach ($csvContent as $k => $csv) {
            $arr = explode(';', $csv[0]);
            $flag = 0;
            foreach ($articuls as $a) {
                if ($a['ARTICUL'] === $arr[0]) {
                    unset($csvContent[$k]);
                    $res[][] = $a['ID'] . ';' . $csv[0];
                    $flag = 1;
                }
            }
            if (!$flag) {
                $res[][] = '0' . ';' . $arr[0] . ';' . $arr[1] . ';0';
            }
        }
        return $res;
    }

    /**
     * @return array
     */
    public function updateTable(): array
    {
        $arr = $this->prepareCsv();
        $csvfile = 'file_prepared.csv';
        $fieldseparator = ";";
        $lineseparator = "\n";
        $fp = fopen('file_prepared.csv', 'w');
        foreach ($arr as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
        try {
            $pdo = new PDO(
                "mysql:host=localhost;dbname=test", DB_USER, DB_PASSWORD,
                array
                (
                    PDO::MYSQL_ATTR_LOCAL_INFILE => true,
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
                )
            );
            $pdo->exec("LOAD DATA LOCAL INFILE " . $pdo->quote($csvfile)
                . "REPLACE INTO TABLE `test`"
                . "FIELDS TERMINATED BY " . $pdo->quote($fieldseparator)
                . "LINES TERMINATED BY " . $pdo->quote($lineseparator)
                . "(ID, ARTICUL, PRICE, COUNT)");
            return $arr;
        } catch (PDOException $e) {
            die("database connection failed: " . $e->getMessage());
        }
    }
}
