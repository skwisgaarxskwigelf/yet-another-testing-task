$(document).ready(function () {
    function refresh(data) {
        $("tbody").empty();
        for (i = 0; i < data.length; i++) {
            data_ = data[i][0].split(';');
            $("tbody").append('<tr><td>' + data_[1] + '</td><td>' + data_[2] + '</td><td>' + data_[3] + '</td></tr>');
        }
    }

    $('button[type=submit]').click(function (event) {
        var formData = new FormData();
        formData.append("CustomField", "This is some extra data");
        var formElement = document.querySelector("form");
        var request = new XMLHttpRequest();
        request.open("POST", "controller.php");
        request.onload = function (oEvent) {
            if (request.status == 200) {
                var error_msg = JSON.parse(request.responseText).error_msg ? JSON.parse(request.responseText).error_msg : '';
                if (error_msg) {
                    alert(error_msg);
                } else {
                    refresh(JSON.parse(request.responseText));
                }
            }
        };
        request.send(new FormData(formElement));
        event.preventDefault();
    });
});
