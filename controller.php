<?php

require_once __DIR__ . "/inputFile.php";

$ext = pathinfo($_FILES['myFile']['name'], PATHINFO_EXTENSION);
if ($ext !== 'csv') {
    $data_sent['error_msg'] = 'wrong extension, csv only';
    echo json_encode($data_sent);
} else {
    $target = "file.".$ext;
    move_uploaded_file($_FILES['myFile']['tmp_name'], $target);
    $obj = new inputFile();
    echo json_encode($obj->updateTable());
}
