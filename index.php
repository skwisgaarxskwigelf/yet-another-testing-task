<!DOCTYPE html>
<html>
<title>
</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="style.css?v=1.001">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<body>
<div class="w3-top test">
</div>
<div class="w3-content" style="max-width:2000px; margin-top:46px">
    <div class="w3-container w3-content w3-center w3-padding-10" style="max-width:800px" id="band">
        <p class="w3-center res" style="min-height: 30px;"></p>
        <div class="w3-row w3-padding-32">
            <form class="form-horizontal">
                <div class="control-group">
                    <div class="controls">
                        <input type="file" id="inputFile" name="myFile" class="form-control">
                    </div>
                </div>
                <button type="submit" id="btnSubmit" class="btn-primary">импортировать</button>
            </form>
        </div>
    </div>
    <div class="w3-gray" id="tour">
        <div class="w3-container w3-content w3-padding-64">
            <table class="table">
                <thead>
                <tr>
                    <th>item</th>
                    <th>price</th>
                    <th>count</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<footer>
</footer>
<script src="main.js?v=1.002"></script>
</body>
</html>
